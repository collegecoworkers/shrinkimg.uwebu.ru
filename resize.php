<?php

function resize ($image, $w_o = false, $h_o = false){
	if(($w_o < 0) || ($h_o < 0)){
		echo "Некорректные входные данные";
		return false;
	}
	list ($w_i, $h_i, $type) = getimagesize( $image);

	$types = array ("", "gif", "jpeg", "png");
	$ext = $types[$type];

	if( $ext ){
		$func = 'imagecreatefrom'.$ext;

		$img_i = $func($image);
	}else{
		echo "Некорректное изображение";
		return false;
	}
	if (!$h_o) $h_o = $w_o / ($w_i / $h_i);
	if (!$w_o) $w_o = $h_o / ($h_i / $w_i);

	$img_o = imagecreatetruecolor($w_o, $h_o);

	$black = imagecolorallocate($img_o, 0, 0, 0);
	imagecolortransparent($img_o, $black);
	imagecopyresampled ($img_o, $img_i, 0,0,0,0, $w_o, $h_o, $w_i, $h_i);

	$func = 'image'.$ext;
	return $func($img_o, $image);
}

$path_file = 'uploads/' . $_FILES['the_file']['name'];
move_uploaded_file($_FILES['the_file']['tmp_name'], $path_file);

resize($path_file, $_POST['width'], $_POST['height']);

header('Location: /'.$path_file);