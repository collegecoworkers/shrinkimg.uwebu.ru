<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Оптимизация изображений</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="assets/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<!-- Static navbar -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="">Оптимизация изображений</a>
				</div>
			</div>
		</nav>
		<form id="form" action="resize.php" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="the_file">Загрузить картинку</label>
						<input type="file" class="form-control-file" id="the_file" name="the_file">
					</div>
					<div class="form-group ">
						<label class="control-label">Ширина</label>
						<input type="text" class="form-control" placeholder="400" value="400" name="width" required>
					</div>
					<div class="form-group ">
						<label class="control-label">Высота</label>
						<input type="text" class="form-control" placeholder="400" value="400" name="height" required>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" id="translate" class="btn btn-success">Перевести</button>
				</div>
			</div>
		</form>
		<br><br><br><br><br>
	</div>
</body>
</html>
